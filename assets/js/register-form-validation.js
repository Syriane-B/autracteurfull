if (document.user_post_form) {
    document.user_post_form.addEventListener("submit", validateRegistrationForm);
}

function validateRegistrationForm(e) {
    e.preventDefault();
    $formErrorBloc = document.getElementById('form-error-bloc');
    $formErrorText = document.getElementById('form-error');
    $formErrorText.innerHTML = '';
    if( document.getElementById('user_post_form_firstName').value == "" ) {
        $formErrorText.append('J\'ai pô bien compris ton prénom');
        $formErrorBloc.classList.remove("hide");
        return;
    }
    if( document.getElementById('user_post_form_lastName').value == "" ) {
        $formErrorText.append('J\'ai pô bien compris ton nom');
        $formErrorBloc.classList.remove("hide");
        return;
    }
    if( document.getElementById('user_post_form_address').value == "" ) {
        $formErrorText.append('Mais c\'est que tu m\'as pas donné une adresse !');
        $formErrorBloc.classList.remove("hide");
        return;
    }
    if( document.getElementById('user_post_form_postCode').value == "" ) {
        $formErrorText.append('T\' aurais pas oublié de m\'dire ton code postal ?');
        $formErrorBloc.classList.remove("hide");
        return;
    }
    if( document.getElementById('user_post_form_city').value == "" ) {
        $formErrorText.append('Mais c\'est que tu m\'as pas donné ta ville !');
        $formErrorBloc.classList.remove("hide");
        return;
    }
    if( document.getElementById('user_post_form_phone').value == "" ) {
        $formErrorText.append('Mais c\'est que tu m\'as pas donné ton 06 !');
        $formErrorBloc.classList.remove("hide");
        return;
    }
    if( document.getElementById('user_post_form_plainPassword_first').value == "" ) {
        $formErrorText.append('Faut choisir un mot passe mon p\'ti !');
        $formErrorBloc.classList.remove("hide");
        return;
    }
    if( !document.getElementById('user_post_form_agreeTerms').value ) {
        $formErrorText.append('Mon p\'ti faut accepter les conditions d\'utilisation pour pouvoir t\'inscrire sur l\'site !');
        $formErrorBloc.classList.remove("hide");
        return;
    } else {
        e.target.submit();
    }
}
