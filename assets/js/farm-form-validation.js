if (document.farm_post_form) {
    document.farm_post_form.addEventListener("submit", validateFarmForm);
}
function validateFarmForm(e) {
    e.preventDefault();
    $formErrorBloc = document.getElementById('form-error-bloc');
    $formErrorText = document.getElementById('form-error');
    $formErrorText.innerHTML = '';
    if( document.getElementById('farm_post_form_name').value == "" ) {
        $formErrorText.append('J\'ai pô bien compris le nom d\'la maison...');
        $formErrorBloc.classList.remove("hide");
        return;
    }
    if( document.getElementById('farm_post_form_siret').value == "" ) {
        $formErrorText.append('J\'crois bien qu\'y a une petite erreur sur le Siret');
        $formErrorBloc.classList.remove("hide");
        return;
    }
    if( document.getElementById('farm_post_form_corporateName').value == "" ) {
        $formErrorText.append('J\'ai pô bien compris le nom officiel de ta companie');
        $formErrorBloc.classList.remove("hide");
        return;
    }
    if( document.getElementById('farm_post_form_address').value == "" ) {
        $formErrorText.append('Y\'a ti pas un quak\' dans l\' adresse ?');
        $formErrorBloc.classList.remove("hide");
        return;
    }
    if( document.getElementById('farm_post_form_postCode').value == "" ) {
        $formErrorText.append('Y\'a ti pas un quak\' dans l\' code postal ?');
        $formErrorBloc.classList.remove("hide");
        return;
    }
    if( document.getElementById('farm_post_form_city').value == "" ) {
        $formErrorText.append('T\'es sur que l\'nom de ta ville est bien correct ?');
        $formErrorBloc.classList.remove("hide");
        return;
    }
    if( document.getElementById('farm_post_form_description').value == "" ) {
        $formErrorText.append('Il m\'senmble que la description est toute Petafinée...');
        $formErrorBloc.classList.remove("hide");
    } else {
        e.target.submit();
    }
}
