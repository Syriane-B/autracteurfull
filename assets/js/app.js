//Entry point to scss files
import '../scss/app.scss';

//deleteProfile modal on myProfilePage
if (document.getElementById('deleteProfileBtn')) {

    let $deleteProfileBtn = document.getElementById('deleteProfileBtn');

    $deleteProfileBtn.addEventListener('click', function (e) {
        document.getElementById('deleteProfileModal').classList.remove('hide');
    });

    document.getElementById('closeModal').addEventListener('click', function (e) {
        document.getElementById('deleteProfileModal').classList.add('hide');
    });
}

//delete farm modal on myFarmPage
if (document.getElementById('deleteFarmBtn')) {
    let $deleteFarmBtn = document.getElementById('deleteFarmBtn');

    $deleteFarmBtn.addEventListener('click', function (e) {
        document.getElementById('deleteFarmModal').classList.remove('hide');
    });
    document.getElementById('closeModal').addEventListener('click', function (e) {
        document.getElementById('deleteFarmModal').classList.add('hide');
    });
}
