<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210112093109 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE farm (id INT AUTO_INCREMENT NOT NULL, owner INT DEFAULT NULL, name VARCHAR(255) NOT NULL, siret BIGINT NOT NULL, corporate_name VARCHAR(255) DEFAULT NULL, address VARCHAR(255) NOT NULL, post_code VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, geo_code VARCHAR(255) DEFAULT NULL, register_date DATE NOT NULL, description LONGTEXT DEFAULT NULL, uploaded_at DATETIME DEFAULT NULL, farm_picture_name VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_5816D045CF60E67C (owner), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mission (id INT AUTO_INCREMENT NOT NULL, farm_id INT NOT NULL, name VARCHAR(255) NOT NULL, starting_date DATE NOT NULL, ending_date DATE NOT NULL, minimum_help_time VARCHAR(255) DEFAULT NULL, description LONGTEXT NOT NULL, rewarding LONGTEXT DEFAULT NULL, INDEX IDX_9067F23C65FCFA0D (farm_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mission_product (mission_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_427690E0BE6CAE90 (mission_id), INDEX IDX_427690E04584665A (product_id), PRIMARY KEY(mission_id, product_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mission_user (mission_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_A4D17A46BE6CAE90 (mission_id), INDEX IDX_A4D17A46A76ED395 (user_id), PRIMARY KEY(mission_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, farm_id INT NOT NULL, name VARCHAR(255) NOT NULL, weight VARCHAR(255) DEFAULT NULL, price DOUBLE PRECISION DEFAULT NULL, picture_file_name VARCHAR(255) DEFAULT NULL, description LONGTEXT NOT NULL, availability TINYINT(1) NOT NULL, uploaded_at DATETIME DEFAULT NULL, INDEX IDX_D34A04AD65FCFA0D (farm_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, post_code VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, phone VARCHAR(255) DEFAULT NULL, register_date DATE NOT NULL, profile_picture VARCHAR(255) NOT NULL, geo_code VARCHAR(255) DEFAULT NULL, uploaded_at DATETIME DEFAULT NULL, is_producer TINYINT(1) NOT NULL, farmOwned INT DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D64926FB1A17 (farmOwned), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_farm (user_id INT NOT NULL, farm_id INT NOT NULL, INDEX IDX_22979C8CA76ED395 (user_id), INDEX IDX_22979C8C65FCFA0D (farm_id), PRIMARY KEY(user_id, farm_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_mission (user_id INT NOT NULL, mission_id INT NOT NULL, INDEX IDX_C86AEC36A76ED395 (user_id), INDEX IDX_C86AEC36BE6CAE90 (mission_id), PRIMARY KEY(user_id, mission_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE farm ADD CONSTRAINT FK_5816D045CF60E67C FOREIGN KEY (owner) REFERENCES user (id)');
        $this->addSql('ALTER TABLE mission ADD CONSTRAINT FK_9067F23C65FCFA0D FOREIGN KEY (farm_id) REFERENCES farm (id)');
        $this->addSql('ALTER TABLE mission_product ADD CONSTRAINT FK_427690E0BE6CAE90 FOREIGN KEY (mission_id) REFERENCES mission (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mission_product ADD CONSTRAINT FK_427690E04584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mission_user ADD CONSTRAINT FK_A4D17A46BE6CAE90 FOREIGN KEY (mission_id) REFERENCES mission (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mission_user ADD CONSTRAINT FK_A4D17A46A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD65FCFA0D FOREIGN KEY (farm_id) REFERENCES farm (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64926FB1A17 FOREIGN KEY (farmOwned) REFERENCES farm (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE user_farm ADD CONSTRAINT FK_22979C8CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_farm ADD CONSTRAINT FK_22979C8C65FCFA0D FOREIGN KEY (farm_id) REFERENCES farm (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_mission ADD CONSTRAINT FK_C86AEC36A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_mission ADD CONSTRAINT FK_C86AEC36BE6CAE90 FOREIGN KEY (mission_id) REFERENCES mission (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE mission DROP FOREIGN KEY FK_9067F23C65FCFA0D');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD65FCFA0D');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64926FB1A17');
        $this->addSql('ALTER TABLE user_farm DROP FOREIGN KEY FK_22979C8C65FCFA0D');
        $this->addSql('ALTER TABLE mission_product DROP FOREIGN KEY FK_427690E0BE6CAE90');
        $this->addSql('ALTER TABLE mission_user DROP FOREIGN KEY FK_A4D17A46BE6CAE90');
        $this->addSql('ALTER TABLE user_mission DROP FOREIGN KEY FK_C86AEC36BE6CAE90');
        $this->addSql('ALTER TABLE mission_product DROP FOREIGN KEY FK_427690E04584665A');
        $this->addSql('ALTER TABLE farm DROP FOREIGN KEY FK_5816D045CF60E67C');
        $this->addSql('ALTER TABLE mission_user DROP FOREIGN KEY FK_A4D17A46A76ED395');
        $this->addSql('ALTER TABLE user_farm DROP FOREIGN KEY FK_22979C8CA76ED395');
        $this->addSql('ALTER TABLE user_mission DROP FOREIGN KEY FK_C86AEC36A76ED395');
        $this->addSql('DROP TABLE farm');
        $this->addSql('DROP TABLE mission');
        $this->addSql('DROP TABLE mission_product');
        $this->addSql('DROP TABLE mission_user');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_farm');
        $this->addSql('DROP TABLE user_mission');
    }
}
