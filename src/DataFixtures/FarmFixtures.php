<?php

namespace App\DataFixtures;

use App\DataFixtures\UserFixtures;
use App\Entity\Farm;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;

class FarmFixtures extends Fixture implements DependentFixtureInterface
{
    public const FARM_REFERENCE = 'farm_';

    public function load(ObjectManager $manager)
    {
        //Fake users
        $faker = Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 10; $i++) {
            $farm = new Farm();
            $farm->setName($faker->company);
            $farm->setSiret((string)$faker->randomNumber(7));
            $farm->setCorporateName($farm->getName() . ' ' . $faker->companySuffix);
            $farm->setAddress($faker->streetAddress);
            $farm->setPostCode($faker->postcode);
            $farm->setCity($faker->city);
            $farm->setRegisterDate($faker->dateTime('now', null));
            $farm->setDescription($faker->text($faker->numberBetween(280,350)));
            $farm->setFarmPictureName('default-farm-picture.jpg');

            $user = $this->getReference(UserFixtures::PRODUCER_REFERENCE.$i);
            $farm->setOwner($user);
            $manager->persist($user);
            $manager->persist($farm);
            $this->addReference(self::FARM_REFERENCE . $i, $farm);
        }

        $manager->flush();
        }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }
}
