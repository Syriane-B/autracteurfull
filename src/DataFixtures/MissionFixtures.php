<?php

namespace App\DataFixtures;

use App\DataFixtures\FarmFixtures;
use App\Entity\Mission;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;

class MissionFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 5; $i++) {
            $mission = new Mission();
            $mission->setName($faker->catchPhrase);
            $mission->setStartingDate($faker->dateTimeThisYear);
            $mission->setEndingDate($faker->dateTimeThisYear);
            $mission->setMinimumHelpTime('1 journée');
            $mission->setDescription($faker->sentence(10));
            $mission->setDescription($faker->sentence(5));
            $farm = $this->getReference(FarmFixtures::FARM_REFERENCE.$i);
            $mission->setFarm($farm);
            $manager->persist($mission);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            FarmFixtures::class,
        );
    }
}
