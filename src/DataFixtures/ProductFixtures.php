<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //        Fake products
        $faker = Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 5; $i++) {
            $product = new Product();
            $product->setName($faker->word);
            $product->setWeight('500g');
            $product->setPrice((float)$faker->randomFloat(2, 2, 20));
            $product->setDescription($faker->sentence(5));
            $product->setAvailability((true));
            $product->setPictureFileName('default-product-picture.jpg');

            $farm = $this->getReference(FarmFixtures::FARM_REFERENCE.$faker->numberBetween(0, 9));
            $farm->addProduct($product);
            $manager->persist($product);
            $manager->persist($farm);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            FarmFixtures::class,
        );
    }
}
