<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public const USER_REFERENCE = 'user_';
    public const PRODUCER_REFERENCE = 'producer_';
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        //        Fake users
        $faker = Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 5; $i++) {
            $user = new User();
            $user->setLastName($faker->name);
            $user->setFirstname($faker->firstName);
            $user->setEmail($faker->email);
            $user->setAddress($faker->streetAddress);
            $user->setPostCode($faker->postcode);
            $user->setCity($faker->city);
            $user->setPhone($faker->phoneNumber);
            $user->setRegisterDate($faker->dateTime('now', null));
            $user->setUploadedAt(new \DateTime('@' . strtotime('now')));
            $user->setPassword(
                $this->passwordEncoder->encodePassword(
                    $user,
                    'password'
                )
            );
            $user->setIsProducer(false);
            //$user->setProfilePicture($faker->image('./public/uploads/userProfilePicture/', 600, 600, null, false));
            $user->setProfilePicture('default-profile-picture.jpg');
            $manager->persist($user);

            $this->addReference(self::USER_REFERENCE . $i, $user);
        }

        //        Fake producers
        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setLastName($faker->name);
            $user->setFirstname($faker->firstName);
            $user->setEmail($faker->email);
            $user->setAddress($faker->streetAddress);
            $user->setPostCode($faker->postcode);
            $user->setCity($faker->city);
            $user->setPhone($faker->phoneNumber);
            $user->setRegisterDate($faker->dateTime('now', null));
            $user->setUploadedAt(new \DateTime('@' . strtotime('now')));
            $user->setPassword(
                $this->passwordEncoder->encodePassword(
                    $user,
                    'password'
                )
            );
            $user->setIsProducer(true);
            $user->setProfilePicture('default-profile-picture.jpg');
            $manager->persist($user);

            $this->addReference(self::PRODUCER_REFERENCE . $i, $user);
        }

        $manager->flush();
    }
}
