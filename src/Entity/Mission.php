<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MissionRepository")
 */
class Mission
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="date")
     */
    private $startingDate;

    /**
     * @ORM\Column(type="date")
     */
    private $endingDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $minimumHelpTime;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $rewarding;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Farm", inversedBy="missions", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $Farm;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Product", inversedBy="missionsRewardingThisProduct",  cascade={"persist"})
     */
    private $rewardProduct;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="missionSignTo",  cascade={"persist"})
     */
    private $helpers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="favoriteMission",  cascade={"persist"})
     */
    private $favoriteUsers;

    public function __construct()
    {
        $this->rewardProduct = new ArrayCollection();
        $this->helpers = new ArrayCollection();
        $this->favoriteUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStartingDate(): ?\DateTimeInterface
    {
        return $this->startingDate;
    }

    public function setStartingDate(\DateTimeInterface $startingDate): self
    {
        $this->startingDate = $startingDate;

        return $this;
    }

    public function getEndingDate(): ?\DateTimeInterface
    {
        return $this->endingDate;
    }

    public function setEndingDate(\DateTimeInterface $endingDate): self
    {
        $this->endingDate = $endingDate;

        return $this;
    }

    public function getMinimumHelpTime(): ?string
    {
        return $this->minimumHelpTime;
    }

    public function setMinimumHelpTime(?string $minimumHelpTime): self
    {
        $this->minimumHelpTime = $minimumHelpTime;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getRewarding(): ?string
    {
        return $this->rewarding;
    }

    public function setRewarding(?string $rewarding): self
    {
        $this->rewarding = $rewarding;

        return $this;
    }

    public function getFarm(): ?Farm
    {
        return $this->Farm;
    }

    public function setFarm(?Farm $Farm): self
    {
        $this->Farm = $Farm;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getRewardProduct(): Collection
    {
        return $this->rewardProduct;
    }

    public function addRewardProduct(Product $rewardProduct): self
    {
        if (!$this->rewardProduct->contains($rewardProduct)) {
            $this->rewardProduct[] = $rewardProduct;
        }

        return $this;
    }

    public function removeRewardProduct(Product $rewardProduct): self
    {
        if ($this->rewardProduct->contains($rewardProduct)) {
            $this->rewardProduct->removeElement($rewardProduct);
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getHelpers(): Collection
    {
        return $this->helpers;
    }

    public function addHelper(User $helper): self
    {
        if (!$this->helpers->contains($helper)) {
            $this->helpers[] = $helper;
        }

        return $this;
    }

    public function removeHelper(User $helper): self
    {
        if ($this->helpers->contains($helper)) {
            $this->helpers->removeElement($helper);
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getFavoriteUsers(): Collection
    {
        return $this->favoriteUsers;
    }

    public function addFavoriteUser(User $favoriteUser): self
    {
        if (!$this->favoriteUsers->contains($favoriteUser)) {
            $this->favoriteUsers[] = $favoriteUser;
            $favoriteUser->addFavoriteMission($this);
        }

        return $this;
    }

    public function removeFavoriteUser(User $favoriteUser): self
    {
        if ($this->favoriteUsers->contains($favoriteUser)) {
            $this->favoriteUsers->removeElement($favoriteUser);
            $favoriteUser->removeFavoriteMission($this);
        }

        return $this;
    }
}
