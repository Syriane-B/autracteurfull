<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @Vich\Uploadable
 */
class Product implements \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $weight;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pictureFileName;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="boolean")
     */
    private $availability;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Farm", inversedBy="products", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $farm;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Mission", mappedBy="rewardProduct")
     */
    private $missionsRewardingThisProduct;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $uploadedAt;

    /**
     * @Vich\UploadableField(mapping="productPicture", fileNameProperty="pictureFileName")
     */
    private $pictureFile;

    public function __construct()
    {
        $this->missionsRewardingThisProduct = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getWeight(): ?string
    {
        return $this->weight;
    }

    public function setWeight($weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPictureFileName(): ?string
    {
        return $this->pictureFileName;
    }

    public function setPictureFileName(?string $pictureFileName): self
    {
        $this->pictureFileName = $pictureFileName;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAvailability(): ?bool
    {
        return $this->availability;
    }

    public function setAvailability(bool $availability): self
    {
        $this->availability = $availability;

        return $this;
    }

    public function getFarm(): ?Farm
    {
        return $this->farm;
    }

    public function setFarm(?Farm $farm): self
    {
        $this->farm = $farm;

        return $this;
    }

    /**
     * @return Collection|Mission[]
     */
    public function getMissionsRewardingThisProduct(): Collection
    {
        return $this->missionsRewardingThisProduct;
    }

    public function addMissionsRewardingThisProduct(Mission $missionsRewardingThisProduct): self
    {
        if (!$this->missionsRewardingThisProduct->contains($missionsRewardingThisProduct)) {
            $this->missionsRewardingThisProduct[] = $missionsRewardingThisProduct;
            $missionsRewardingThisProduct->addRewardProduct($this);
        }

        return $this;
    }

    public function removeMissionsRewardingThisProduct(Mission $missionsRewardingThisProduct): self
    {
        if ($this->missionsRewardingThisProduct->contains($missionsRewardingThisProduct)) {
            $this->missionsRewardingThisProduct->removeElement($missionsRewardingThisProduct);
            $missionsRewardingThisProduct->removeRewardProduct($this);
        }

        return $this;
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->name,
            $this->weight,
            $this->price
        ));
    }
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->name,
            $this->weight,
            $this->price
            ) = unserialize($serialized);
    }

    public function getUploadedAt(): ?\DateTimeInterface
    {
        return $this->uploadedAt;
    }

    public function setUploadedAt(?\DateTimeInterface $uploadedAt): self
    {
        $this->uploadedAt = $uploadedAt;

        return $this;
    }

    public function getPictureFile(): ?File
    {
        return $this->pictureFile;
    }

    public function setPictureFile(?File $pictureFile = null): void
    {
        $this->pictureFile = $pictureFile;
        if ($pictureFile) {
            $this->uploadedAt = new DateTime('now');
        }
    }
}
