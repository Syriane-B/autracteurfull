<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity (repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity (fields={"email"}, message="Mais je t'connais d'jà toi, oui oui, avec cet email ci !")
 * @Vich\Uploadable
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string")
     */
    private $postCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="date")
     */
    private $registerDate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $profilePicture;

    /**
     * @Vich\UploadableField(mapping="userProfilePicture", fileNameProperty="profilePicture")
     * @var File
     */
    private $profilePictureFile;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $geoCode;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Mission", mappedBy="helpers")
     */
    private $missionSignTo;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Farm")
     */
    private $favoriteFarm;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Mission", inversedBy="favoriteUsers")
     */
    private $favoriteMission;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $uploadedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isProducer;

    /**
     * @ORM\OneToOne(targetEntity=Farm::class, mappedBy="owner", cascade={"persist"})
     * @ORM\JoinColumn(name="farmOwned", referencedColumnName="id", onDelete="SET NULL")
     */
    private $farmOwned;

    public function __construct()
    {
        $this->missionSignTo = new ArrayCollection();
        $this->favoriteFarm = new ArrayCollection();
        $this->favoriteMission = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPostCode(): ?string
    {
        return $this->postCode;
    }

    public function setPostCode(string $postCode): self
    {
        $this->postCode = $postCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getRegisterDate(): ?\DateTimeInterface
    {
        return $this->registerDate;
    }

    public function setRegisterDate(\DateTimeInterface $registerDate): self
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    public function getProfilePicture(): ?string
    {
        return $this->profilePicture;
    }

    public function setProfilePicture(?string $profilePicture): self
    {
        $this->profilePicture = $profilePicture;

        return $this;
    }

    /**
     * @return File
     */
    public function getProfilePictureFile(): ?File
    {
        return $this->profilePictureFile;
    }

    /**
     * @param File $profilePictureFile
     * @throws Exception
     */
    public function setProfilePictureFile(?File $profilePictureFile = null): void
    {
        $this->profilePictureFile = $profilePictureFile;
        if ($profilePictureFile) {
            $this->uploadedAt = new DateTime('now');
        }
    }


    public function getGeoCode(): ?string
    {
        return $this->geoCode;
    }

    public function setGeoCode(string $geoCode): self
    {
        $this->geoCode = $geoCode;

        return $this;
    }

    /**
     * @return Collection|Mission[]
     */
    public function getMissionSignTo(): Collection
    {
        return $this->missionSignTo;
    }

    public function addMissionSignTo(Mission $missionSignTo): self
    {
        if (!$this->missionSignTo->contains($missionSignTo)) {
            $this->missionSignTo[] = $missionSignTo;
            $missionSignTo->addHelper($this);
        }

        return $this;
    }

    public function removeMissionSignTo(Mission $missionSignTo): self
    {
        if ($this->missionSignTo->contains($missionSignTo)) {
            $this->missionSignTo->removeElement($missionSignTo);
            $missionSignTo->removeHelper($this);
        }

        return $this;
    }

    /**
     * @return Collection|Farm[]
     */
    public function getFavoriteFarm(): Collection
    {
        return $this->favoriteFarm;
    }

    public function addFavoriteFarm(Farm $favoriteFarm): self
    {
        if (!$this->favoriteFarm->contains($favoriteFarm)) {
            $this->favoriteFarm[] = $favoriteFarm;
        }

        return $this;
    }

    public function removeFavoriteFarm(Farm $favoriteFarm): self
    {
        if ($this->favoriteFarm->contains($favoriteFarm)) {
            $this->favoriteFarm->removeElement($favoriteFarm);
        }

        return $this;
    }

    /**
     * @return Collection|Mission[]
     */
    public function getFavoriteMission(): Collection
    {
        return $this->favoriteMission;
    }

    public function addFavoriteMission(Mission $favoriteMission): self
    {
        if (!$this->favoriteMission->contains($favoriteMission)) {
            $this->favoriteMission[] = $favoriteMission;
        }

        return $this;
    }

    public function removeFavoriteMission(Mission $favoriteMission): self
    {
        if ($this->favoriteMission->contains($favoriteMission)) {
            $this->favoriteMission->removeElement($favoriteMission);
        }

        return $this;
    }

    public function getUploadedAt(): ?\DateTimeInterface
    {
        return $this->uploadedAt;
    }

    public function setUploadedAt(?\DateTimeInterface $uploadedAt): self
    {
        $this->uploadedAt = $uploadedAt;

        return $this;
    }

    public function getIsProducer(): ?bool
    {
        return $this->isProducer;
    }

    public function setIsProducer(bool $isProducer): self
    {
        $this->isProducer = $isProducer;

        return $this;
    }

    public function getFarmOwned(): ?Farm
    {
        return $this->farmOwned;
    }

    public function setFarmOwned(?Farm $farmOwned): self
    {
        $this->farmOwned = $farmOwned;

        // set (or unset) the owning side of the relation if necessary
        if($farmOwned) {
            if ($farmOwned->getOwner() !== $this) {
                $farmOwned->setOwner($this);
            }
    }

        return $this;
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->firstName,
            $this->lastName,
            $this->password,
            $this->email
        ));
    }
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->firstName,
            $this->lastName,
            $this->password,
            $this->email
            ) = unserialize($serialized);
    }


}
