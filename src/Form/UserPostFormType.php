<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Vich\UploaderBundle\Form\Type\VichImageType;

class UserPostFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'required' => true,
                'invalid_message' => 'Mais c\' est que tu m\'as pas donné un mail valide !',
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'label' => 'Veuillez accepter nos conditions d\'utilisation',
                'required' => true,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Mon p\'ti faut accepter les conditions d\'utilisation pour pouvoir t\'inscrire sur l\'site !',
                    ]),
                ],
            ])
            ->add('firstName', TextType::class, [
                'invalid_message' => 'J\'ai pô bien compris ton prénom',
                'label' => 'Prénom',
                'required' => true,
            ])
            ->add('lastName', TextType::class, [
                'invalid_message' => 'J\'ai pô bien compris ton nom',
                'label' => 'Nom',
                'required' => true,
            ])
            ->add('address', TextType::class, [
                'label' => 'Adresse',
                'required' => true,
            ])
            ->add('postCode', TextType::class, [
                'label' => 'Code postal',
                'required' => true,
            ])
            ->add('city', TextType::class, [
                'label' => 'Ville',
                'required' => true,
            ])
            ->add('phone', TextType::class, [
                'label' => 'Téléphone',
                'required' => true,
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Faut choisir un mot passe mon p\'ti !',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Faut qu\'ton mot passe ai au moins 6 caractères !',
                        'max' => 255,
                    ]),
                ],
                'required' => true,
                'first_options' => ['label' => 'Mot de passe'],
                'second_options' => ['label' => 'Validation du mot de passe'],
            ])
            ->add('profilePictureFile', VichImageType::class,
                [
                    'image_uri' => false,
                    'allow_delete' => false,
                    'download_uri' => false,
                    'required' => false,
                    'label' => 'Sélectionnez votre image de profile',

                ]
            )
            ->add('isProducer', CheckboxType::class, [
                'required' => false,
                'label' => 'Etes vous un producteur ?',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
