<?php

namespace App\Form;

use App\Entity\Farm;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class FarmPutFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'invalid_message' => 'J\'ai pô bien compris le nom d\'la maison...',
                'label' => 'Nom de l\'établissement',
                'required' => false,
            ])
            ->add('siret', NumberType::class, [
                'invalid_message' => 'J\'crois bien qu\'y a une petite erreur sur le Siret',
                'label' => 'Siret',
                'required' => false,
            ])
            ->add('corporateName', TextType::class, [
                'invalid_message' => 'J\'ai pô bien compris le nom officiel de ta companie',
                'label' => 'Raison sociale de l\'établissement',
                'required' => false,
            ])
            ->add('address', TextType::class, [
                'invalid_message' => 'Y\'a ti pas un quak\' dans l\' adresse ?',
                'label' => 'Adresse de l\'établissement',
                'required' => false,
            ])
            ->add('postCode', NumberType::class, [
                'invalid_message' => 'Y\'a ti pas un quak\' dans l\' code postal ?',
                'label' => 'Code postal de l\'établissement',
                'required' => false,
            ])
            ->add('city', TextType::class, [
                'invalid_message' => 'T\'es sur que l\'nom de ta ville est bien correct ?',
                'label' => 'Ville de l\'établissement',
                'required' => false,
            ])
            ->add('description', TextareaType::class, [
                'invalid_message' => 'Il m\'senmble que la description est toute Petafinée...',
                'label' => 'Desription de l\'établissement',
                'required' => false,
                'attr' => ['rows' => '5'],
            ])
            ->add('farmPicture', VichImageType::class,
                [
                    'image_uri' => false,
                    'allow_delete' => false,
                    'download_uri' => false,
                    'required' => false,
                    'label' => 'Sélectionnez l\'image de votre ferme',
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Farm::class,
        ]);
    }
}
