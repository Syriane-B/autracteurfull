<?php

namespace App\Form;

use App\Entity\Product;
use Doctrine\DBAL\Types\FloatType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ProductPostFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'invalid_message' => 'J\'ai pô bien compris le nom d\'ton produit...',
                'label' => 'Nom du produit',
                'required' => true,
            ])
            ->add('weight', TextType::class, [
                'invalid_message' => 'Redis le poids',
                'label' => 'Poids de conditionnement'
            ])
            ->add('price', NumberType::class, [
                'invalid_message' => 'Dis voir le prix en chiffre',
                'label' => 'Prix'
            ])
            ->add('description', TextType::class, [
                'invalid_message' => 'Explique avec tes mots c\'que c\'est le produit',
                'label' => 'C\'est quoi le petit plus de ce produit ?',
                'required' => true,
            ])
            ->add('pictureFile', VichImageType::class,
                [
                    'image_uri' => false,
                    'allow_delete' => false,
                    'download_uri' => false,
                    'required' => false,
                    'label' => 'Sélectionnez l\'image de votre produit',
                ]
            )
            ->add('availability', CheckboxType::class, [
                'required' => false,
                'label' => 'Produit dispo !',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
