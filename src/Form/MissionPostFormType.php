<?php

namespace App\Form;

use App\Entity\Mission;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MissionPostFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'invalid_message' => 'J\'ai pô bien compris le nom d\'la mission...',
                'label' => 'Nom de la mission',
                'required' => true,
            ])
            ->add('startingDate', DateType::class, [
                'invalid_message' => 'met bien la date comme il faut : 01/01/2020',
                'label' => 'Début de la mission',
                'format' => 'dd-MM-yyyy',
                'required' => true,
            ])
            ->add('endingDate', DateType::class, [
                'invalid_message' => 'met bien la date comme il faut : 01/01/2020',
                'label' => 'Fin de la mission',
                'format' => 'dd-MM-yyyy',
                'required' => true,
            ])
            ->add('minimumHelpTime', TextType::class, [
                'invalid_message' => 'Met une indication sur le temps minimum qu\'il faut pour t\'aider',
                'label' => 'Temps minimum pour aider',
                'required' => true,
            ])
            ->add('description', TextType::class, [
                'invalid_message' => 'Explique avec tes mots c\'qu\'on va faire',
                'label' => 'Qu\'est ce qu\'on va faire ?',
                'required' => true,
            ])
            ->add('rewarding', TextType::class, [
                'invalid_message' => 'Ecris le petit cadeux avec tes mots',
                'label' => 'Dis-y voir le petit cadeaux en échange du coup de main',
                'required' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Mission::class,
        ]);
    }
}
