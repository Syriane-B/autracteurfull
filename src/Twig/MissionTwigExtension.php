<?php

namespace App\Twig;

use App\Entity\Mission;
use App\Entity\User;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class MissionTwigExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('isUserLikeThisMission', [$this, 'isUserLikeThisMission']),
            new TwigFunction('isUserSignForThisMission', [$this, 'isUserSignForThisMission']),
        ];
    }

    public function isUserLikeThisMission(Mission $mission, User $user)
    {
        $isUserLike = false;
        if (count($user->getFavoriteMission()) > 0) {
            foreach ($user->getFavoriteMission() as $favMission) {
                if ($favMission->getId() === $mission->getId()) {
                    $isUserLike = true;
                }
            }
        }
        return $isUserLike;
    }

    public function isUserSignForThisMission(Mission $mission, User $user)
    {
        $isUserSign = false;
        if (count($user->getMissionSignTo()) > 0) {
            foreach ($user->getMissionSignTo() as $signMission) {
                if ($signMission->getId() === $mission->getId()) {
                    $isUserSign = true;
                }
            }
        }
        return $isUserSign;
    }
}
