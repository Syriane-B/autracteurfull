<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserPutFormType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user")
 * @IsGranted("ROLE_USER")
 */

class UserController extends AbstractController
{
    private $userRepository;
    private $entityManager;

    public function __construct(UserRepository $userRepository, EntityManagerInterface $entityManager)
    {
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/my", name="myProfile")
     */
    public function myProfile()
    {
        return $this->render('user/myProfile.html.twig', [
            'controller_name' => 'UserController',
            'isMyMission' => false,
            'isMyProduct' => false,
        ]);
    }

    /**
     * @Route("/delete", name="deleteProfile")
     * @param Request $request
     * @return RedirectResponse
     */
    public function deleteProfile(Request $request)
    {
        //get the user to delete
        $connectedUser = $this->getUser();

        $this->entityManager->remove($connectedUser);
        $this->entityManager->flush();

        //logout before redirecting
        $this->get('security.token_storage')->setToken(null);
        $request->getSession()->invalidate();

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/edit", name="modifyProfile")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return RedirectResponse|Response
     */
    public function modifyProfile(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = $this->getUser();
        $form = $this->createForm(UserPutFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // if new password given, encode the plain password
            if ($form->get('plainPassword')->getData() !== null){
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $form->get('plainPassword')->getData()
                    )
                );
            }
            $user->setGeoCode('GPS coordonates');
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // do anything else you need here, like send an email

            return $this->redirectToRoute('myProfile');
        }

        return $this->render('user/modifyProfileInfo.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
