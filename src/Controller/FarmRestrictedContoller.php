<?php

namespace App\Controller;

use App\Entity\Farm;
use App\Form\FarmPostFormType;
use App\Form\FarmPutFormType;
use App\Repository\FarmRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/farm")
 * @IsGranted("ROLE_USER")
 */
class FarmRestrictedContoller extends AbstractController
{
    private $farmRepository;
    private $entityManager;

    public function __construct(FarmRepository $farmRepository, EntityManagerInterface $entityManager)
    {
        $this->farmRepository = $farmRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/new", name="newFarm")
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function newFarm(Request $request): Response
    {
        $farm = new Farm();
        $form = $this->createForm(FarmPostFormType::class, $farm,
            array(
                'attr'=>array('novalidate'=>'novalidate')
            ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //set the producer user
            $connectedUser = $this->getUser();
            $farm->setOwner($connectedUser);
            //set registration date
            $farm->setRegisterDate(new DateTime('NOW'));
            //set default farmPicture if the users didn't uploaded one (the file upload is handeled by Vich Uploader)
            $pictureFile = $form['farmPicture']->getData();
            if (empty($pictureFile)) {
                $farm->setFarmPictureName('default-farm-picture.jpg');
                $farm->setUploadedAt(new DateTime('NOW'));
            }
            $this->entityManager->persist($farm);
            $this->entityManager->flush();

            return $this->redirectToRoute('farm', [
                'id' => $farm->getId()
            ]);
        }

        return $this->render('farm/farmPost.html.twig', [
            'farm' => $farm,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="editFarm", methods={"GET","POST"})
     * @param Farm $farm
     * @param Request $request
     * @return Response
     */
    public function editFarm(Farm $farm, Request $request): Response
    {
        $form = $this->createForm(FarmPostFormType::class, $farm,
            array(
                'attr'=>array('novalidate'=>'novalidate')
            ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('myFarm');
        }

        return $this->render('farm/farmPut.html.twig', [
            'farm' => $farm,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="deleteFarm", methods={"GET"})
     */
    public function delete(Farm $farm, Request $request): Response
    {
        $connectedUser = $this->getUser();
        $connectedUser->setFarmOwned(null);
        $this->entityManager->remove($farm);
        $this->entityManager->flush();

        return $this->redirectToRoute('myProfile');
    }

    /**
     * @Route("/my", name="myFarm")
     */
    public function myFarm(): Response
    {
        $farm = $this->getUser()->getFarmOwned();
        return $this->render('farm/myFarmPage.html.twig', [
            'farm' => $farm,
        ]);
    }

    /**
     * @Route("/addFavorite/{id}", name="addFavoriteFarm")
     * @param Farm $farm
     * @param Request $request
     * @return RedirectResponse
     */
    public function addFavoriteFarm(Farm $farm, Request $request)
    {
        $connectedUser = $this->getUser();

        //add the farm to connected user favorite farm
        $connectedUser->addFavoriteFarm($farm);
        $this->entityManager->persist($connectedUser);
        $this->entityManager->flush();

        //redirect to previous route
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);

    }

    /**
     * @Route("/removeFavorite/{id}", name="removeFavoriteFarm")
     * @param Farm $farm
     * @param Request $request
     * @return RedirectResponse
     */
    public function removeFavoriteFarm(Farm $farm, Request $request)
    {
        $connectedUser = $this->getUser();

        //add the farm to connected user favorite farm
        $connectedUser->removeFavoriteFarm($farm);
        $this->entityManager->persist($connectedUser);
        $this->entityManager->flush();

        //redirect to previous route
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);

    }
}
