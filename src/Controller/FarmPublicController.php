<?php

namespace App\Controller;

use App\Entity\Farm;
use App\Repository\FarmRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/farm")
 */
class FarmPublicController extends AbstractController
{
    private $farmRepository;
    private $entityManager;

    public function __construct(FarmRepository $farmRepository, EntityManagerInterface $entityManager)
    {
        $this->farmRepository = $farmRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/all", name="findFarm")
     */
    public function findFarm(): Response
    {
        $farms = $this->farmRepository->findAll();
        return $this->render('farm/findFarm.html.twig', [
            'farms' => $farms,
        ]);
    }


    /**
     * @Route("/page/{id}", name="farm")
     * @param Farm $farm
     * @return ResponseK
     */
    public function farm(Farm $farm)
    {
        //get the farm
        $connectedUser = $this->getUser();
        //check if connected user follow the farm
        $connectedUserIsAlreadyLikeThisFarm = false;
        if ($connectedUser !== null) {
            foreach ( $connectedUser->getFavoriteFarm() as $favoriteFarm) {
                if ($favoriteFarm->getId() == $farm->getId()){
                    $connectedUserIsAlreadyLikeThisFarm = true;
                }
            }
        }

        return $this->render('farm/farmPage.html.twig', [
            'farm' => $farm,
            'connectedUserIsAlreadyLikeThisFarm' => $connectedUserIsAlreadyLikeThisFarm
        ]);
    }
}
