<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductPostFormType;
use App\Repository\ProductRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/product")
 * @IsGranted("ROLE_USER")
 */
class ProductRestrictedController extends AbstractController
{
    private $productRepository;
    private $entityManager;

    public function __construct(ProductRepository $productRepository, EntityManagerInterface $entityManager)
    {
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/new", name="newProduct")
     * @param Request $request
     * @return Response
     */
    public function newProduct(Request $request): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductPostFormType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //set the farm of the product
            $farm =$this->getUser()->getFarmOwned();
            $product->setFarm($farm);

            //set default productPicture if the users didn't uploaded one (the file upload is handeled by Vich Uploader)
            $pictureFile = $form['pictureFile']->getData();
            if (empty($pictureFile)) {
                $product->setPictureFileName('default-product-picture.jpg');
                $product->setUploadedAt(new DateTime('NOW'));
            }

            $this->entityManager->persist($product);
            $this->entityManager->flush();

            return $this->redirectToRoute('myFarm');
        }
        return $this->render('product/productPost.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }
}
