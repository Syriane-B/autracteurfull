<?php

namespace App\Controller;

use App\Entity\Mission;
use App\Form\MissionPostFormType;
use App\Form\ProductPostFormType;
use App\Repository\MissionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/mission")
 * @IsGranted("ROLE_USER")
 */
class MissionController extends AbstractController
{
    private $missionRepository;
    private $entityManager;

    public function __construct(MissionRepository $missionRepository, EntityManagerInterface $entityManager)
    {
        $this->missionRepository = $missionRepository;
        $this->entityManager = $entityManager;
    }



    /**
     * @Route("/new", name="newMission")
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function newMission(Request $request): Response
    {
        $mission = new Mission();
        $form = $this->createForm(MissionPostFormType::class, $mission);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //set the producer user
            $connectedUser = $this->getUser();
            $farm = $connectedUser->getFarmOwned();
            $mission->setFarm($farm);

            $this->entityManager->persist($mission);
            $this->entityManager->flush();

            return $this->redirectToRoute('myFarm');
        }
        return $this->render('mission/missionPost.html.twig', [
            'mission' => $mission,
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/editMission/{missionId}", name="editMission", methods={"GET","POST"})
     * @param Request $request
     * @param int $missionId
     * @return Response
     */
    public function editMission(Request $request, int $missionId): Response
    {
        $mission = $this->missionRepository->find($missionId);
        $form = $this->createForm(MissionPostFormType::class, $mission);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('myFarm');
        }

        return $this->render('mission/missionPost.html.twig', [
            'mission' => $mission,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/all", name="findMission")
     */
    public function findMission(): Response
    {
        $missions = $this->missionRepository->findAll();
        return $this->render('mission/findMission.html.twig', [
            'missions' => $missions,
            'isMyMission' => false,
        ]);
    }

    /**
     * @Route("/addFavorite/{id}", name="addFavoriteMission")
     * @param Mission $mission
     * @param Request $request
     * @return RedirectResponse
     */
    public function addFavoriteMission(Mission $mission, Request $request)
    {
        $connectedUser = $this->getUser();

        //add the farm to connected user favorite farm
        $connectedUser->addFavoriteMission($mission);
        $this->entityManager->persist($connectedUser);
        $this->entityManager->flush();

        //redirect to previous route
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);

    }

    /**
     * @Route("/removeFavorite/{id}", name="removeFavoriteMission")
     * @param Mission $mission
     * @param Request $request
     * @return RedirectResponse
     */
    public function removeFavoriteMission(Mission $mission, Request $request)
    {
        $connectedUser = $this->getUser();

        //add the farm to connected user favorite farm
        $connectedUser->removeFavoriteMission($mission);
        $this->entityManager->persist($connectedUser);
        $this->entityManager->flush();

        //redirect to previous route
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);

    }

    /**
     * @Route("/signIn/{id}", name="signInMission")
     * @param Mission $mission
     * @param Request $request
     * @return RedirectResponse
     */
    public function signInMission(Mission $mission, Request $request)
    {
        $connectedUser = $this->getUser();

        //add the farm to connected user favorite farm
        $connectedUser->addMissionSignTo($mission);
        $this->entityManager->persist($connectedUser);
        $this->entityManager->flush();

        //redirect to previous route
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);

    }

    /**
     * @Route("/unSign/{id}", name="unSignMission")
     * @param Mission $mission
     * @param Request $request
     * @return RedirectResponse
     */
    public function unSignMission(Mission $mission, Request $request)
    {
        $connectedUser = $this->getUser();

        //add the farm to connected user favorite farm
        $connectedUser->removeMissionSignTo($mission);
        $this->entityManager->persist($connectedUser);
        $this->entityManager->flush();

        //redirect to previous route
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);

    }
    /**
     * @Route("/deleteMission/{missionId}", name="deleteMission", methods={"GET"})
     */
    public function deleteMission(Request $request, int $missionId): Response
    {
        $mission = $this->missionRepository->find($missionId);

        $this->entityManager->remove($mission);
        $this->entityManager->flush();

        return $this->redirectToRoute('myFarm');
    }

    /**
     * @Route("/{id}", name="mission")
     * @param Mission $mission
     * @return Response
     */
    public function mission(Mission $mission)
    {
        return $this->render('mission/missionPage.html.twig', [
            'mission' => $mission,
        ]);
    }

}
